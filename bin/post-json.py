#!/usr/bin/env python
import os
import json
import argparse
import urllib2


placemark_type = "{http://www.opengis.net/kml/2.2}Placemark"
track_type = "{http://www.google.com/kml/ext/2.2}Track"
coord_type = "{http://www.google.com/kml/ext/2.2}coord"
when_type = "{http://www.opengis.net/kml/2.2}when"



"""
position = {
    latitude: 0,
    longitude: 0,
    altitude: 0,
    altitudeAccuracy: 0,
    heading: 0,
    speed: 0,
    timestamp: 0
}
"""
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--input','-i', help='Kml inputs', required=True)
    parser.add_argument('--url', '-u', help='Post to url', required=True)
    args = parser.parse_args()

    with open(args.input) as jsonFile:
        data = json.load(jsonFile)
        req = urllib2.Request(args.url)
        req.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(req, json.dumps({'routePoints':data}))
        print "\nResponse:"
        print response.read()

