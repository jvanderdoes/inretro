#!/usr/bin/env python
import os
import json
import argparse
import xml.etree.ElementTree as ET


placemark_type = "{http://www.opengis.net/kml/2.2}Placemark"
track_type = "{http://www.google.com/kml/ext/2.2}Track"
coord_type = "{http://www.google.com/kml/ext/2.2}coord"
when_type = "{http://www.opengis.net/kml/2.2}when"



"""
position = {
    latitude: 0,
    longitude: 0,
    altitude: 0,
    altitudeAccuracy: 0,
    heading: 0,
    speed: 0,
    timestamp: 0
}
"""
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--input','-i', nargs="+", help='Kml inputs', required=True)
    parser.add_argument('--out-dir', '-o', help='Output directory', required=True)
    
    args = parser.parse_args()
    for xml_path in args.input:
        tree = ET.parse(xml_path)
        root = tree.getroot()
        document = root.getchildren()
        routes = []
        for doc in document:
            placemarks = doc.findall(placemark_type)
            for placemark in placemarks:
                tracks = placemark.findall(track_type)
                for track in tracks:
                    whens = track.findall(when_type)
                    coords = track.findall(coord_type)
                    for when, coord in zip(whens,coords):
                        digits = [float(i) for i in coord.text.split()]

                        position = {}
                        position['time'] = when.text
                        position['longitude'] = digits[0]
                        position['latitude'] = digits[1]
                        position['elevationAccuracy'] = digits[2]
                        position['altitudeAccuracy'] = 0
                        position['heading'] = 0
                        position['speed'] = 0
                        routes.append(position)

        basename = os.path.basename(xml_path)
        file_name, file_extension = os.path.splitext(basename)
        output = os.path.join(args.out_dir,file_name)+".json"
        with open(output,"w") as jsonOut:
            json.dump(routes, jsonOut, indent=4)
