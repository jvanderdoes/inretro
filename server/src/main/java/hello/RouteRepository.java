
package hello;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "routes", path = "routes")
public interface RouteRepository extends CrudRepository<Route, Long> {

	List<Route> findByLastName(@Param("name") String name);

}
