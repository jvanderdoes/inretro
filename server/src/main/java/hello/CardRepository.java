
package hello;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "cards", path = "cards")
public interface CardRepository extends CrudRepository<Card, Long> {

	List<Card> findByLastName(@Param("name") String name);
	
	List<Card> findByRouteId(@Param("id") Long routeId);
	
	List<Card> findByDatetime(@Param("id") Long datetime);

}
