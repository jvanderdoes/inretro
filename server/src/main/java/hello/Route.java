package hello;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.gemfire.mapping.Region;

@Region("hello")
public class Route {

	private static AtomicLong COUNTER = new AtomicLong(0L);

	@Id private Long id;

	private String firstName;
	private String lastName;
	private ArrayList<Waypoint> routePoints;

	@PersistenceConstructor
	public Route() {
		this.id = COUNTER.incrementAndGet();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getRoutePoints() {
		return routePoints.toString();
	}
	
	public void setRoutePoints(ArrayList<Waypoint> routePoints) {
		this.routePoints = routePoints;
	}
	
	public void addRoutePoint(Waypoint waypoint) {
		if(routePoints == null) {
			routePoints = new ArrayList<Waypoint>();
		}
		routePoints.add(waypoint);
	}


}
