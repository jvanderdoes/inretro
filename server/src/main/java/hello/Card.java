package hello;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.gemfire.mapping.Region;




@Region("hello")
public class Card {

	private static AtomicLong COUNTER = new AtomicLong(0L);

	@Id private Long id;

	private String firstName;
	private String lastName;

	private String type;
	private String description;
	private Waypoint location;
	private Date datetime;
	


	private Long routeId;
	private byte[] img;


	@PersistenceConstructor
	public Card() {
		this.id = COUNTER.incrementAndGet();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Waypoint getLocation() {
		return location;
	}

	public void setLocation(Waypoint location) {
		this.location = location;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public Long getRouteId() {
		return routeId;
	}

	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "Card [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", type=" + type + ", description=" + description
				+ ", location=" + location + ", datetime=" + datetime
				+ ", routeId=" + routeId + ", img=" + Arrays.toString(img)
				+ "]";
	}

	
	
}
