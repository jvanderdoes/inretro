package hello;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
/*
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
*/


@RepositoryEventHandler(Route.class)

public class RouteEventHandler {

	
	private List<HttpMessageConverter<?>> getMessageConverters() {
		
		System.out.println("Jackson Supported Media Types" + new MappingJackson2HttpMessageConverter().getSupportedMediaTypes().toString());
		System.out.println("Source Http Supported Media Types" + new SourceHttpMessageConverter().getSupportedMediaTypes().toString());
		System.out.println("Form Http Supported Media Types" + new FormHttpMessageConverter().getSupportedMediaTypes().toString());
		
		MappingJackson2HttpMessageConverter jmc = new MappingJackson2HttpMessageConverter();
		ObjectMapper om = jmc.getObjectMapper();
		System.out.println("ObjectMapper " + om.toString());
		System.out.println("CanRead " + jmc.canRead(PoiList.class, new MediaType("UTF-8")));

		
	    List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
	    converters.add(new MappingJackson2HttpMessageConverter());
//	    converters.add(new SourceHttpMessageConverter<>());
//	    converters.add(new FormHttpMessageConverter());
	    
	    return converters;
	}
	
    //private static final String endpoint = "


	  @HandleBeforeDelete public void handleBeforeDelete(Route p) {
		  System.out.println("handleBeforeDelete");
	  }

	  @HandleAfterCreate public void handleAfterCreate(Route p) {
		  System.out.println("HandleAfterCreate");

		  RestTemplate restTemplate = new RestTemplate();
		  
		  restTemplate.setMessageConverters(this.getMessageConverters());
          
//		  String url = new String().concat(endpoint) + "
//		  String url = new String().concat(endpoint) + "find?text=starbucks&outFields=Type%2CCity%2CRegion%2CCountry&maxLocations=20&location=-78.890096%2C35.055167&distance=10000&f=pjson";		  
		  
		  UriTemplate uriTemplate = new UriTemplate("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find?text={searchTerms}&maxLocations={maxLocations}&location={location}&distance={distance}&f={format}");
				  
				  
//				  "http://example.com/hotels/{hotel}/bookings/{booking}");
		  Map<String, String> uriVariables = new HashMap<String, String>();
		  uriVariables.put("searchTerms", "starbucks");
		  uriVariables.put("maxLocations", "10");
		  uriVariables.put("location", "-78.890096,35.055167");
		  uriVariables.put("distance", "10000");
		  uriVariables.put("format", "pjson");	
		  System.out.println("uriTemplate = " + uriTemplate.expand(uriVariables));

		  
/*		  
		  HttpClient client = new DefaultHttpClient();
		  HttpGet post = new HttpPost(uriTemplate.expand(uriVariables))

		    StringEntity input = new StringEntity('product');
		    post.setEntity(input);
		    HttpResponse response = client.execute(post);
		    BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		    String line = '';
		  
		    while ((line = rd.readLine()) != null) {
		  		     System.out.println(line);
	  		    }	
		   }
*/
		  
		  
//		  System.out.println("url is " + url);


		  PoiList poiList = restTemplate.getForObject(uriTemplate.expand(uriVariables), PoiList.class);
		  System.out.println("card is " + poiList.toString());
		  
		  //public transport alternative -- entire path
		  
		  //places of interest -- waypoint
		  
		  //events  -- waypoint
		  
		  //businesses  -- waypoint
		  
		  //arts & culture  -- waypoint
		
		  
		  
	  }

}
