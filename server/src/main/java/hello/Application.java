package hello;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.gemfire.CacheFactoryBean;
import org.springframework.data.gemfire.LocalRegionFactoryBean;
import com.gemstone.gemfire.cache.GemFireCache;
import org.springframework.context.annotation.Bean;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.gemfire.listener.ContinuousQueryListenerContainer;

//@ComponentScan
//@Configuration
@EnableGemfireRepositories
@Import(RepositoryRestMvcConfiguration.class)
@EnableAutoConfiguration
public class Application {


	@Bean
	CacheFactoryBean cacheFactoryBean() {
		return new CacheFactoryBean();
	}

	@Bean
	LocalRegionFactoryBean<String, Card> localRegionFactory(final GemFireCache cache) {
		return new LocalRegionFactoryBean<String, Card>() {{
			setCache(cache);
			setName("hello");
		}};
	}
	
	@Bean RouteEventHandler routeEventHandler() {
	    return new RouteEventHandler();
	  }	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    
    
    
}


