# InRetro
## View your life in retrospect
Here's how you can support this project:

Follow this Project and get updates.
Angelenos routinely traverse the city day after day, often traveling great distances via freeway, with little idea of what lies between points. 

InRetro is based on the observation that your quality of life in LA vastly improves when you work close to home or otherwise have a happy commute.     

Rather than giving prescriptive advice on how to improve your life, InRetro helps you reflect on your day's journey and highlights alternatives. Users provide profile info that includes home neighborhood and interests. InRetro monitors your mobile GPS info and matches it against your profile to foster self-reflection, for example:
"You traveled 26 miles today."  
"Your journey would have taken 4 hours and cost $3, via public transportation."
"Next time, consider a visit to Pizza Bravo."  
"Check out this job opening 5 miles from your home."

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact