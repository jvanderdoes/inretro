function calcDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var radlon1 = Math.PI * lon1/180
    var radlon2 = Math.PI * lon2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
}

var processWayPoints = function($http, data, callback){

     var esriRoot = "http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/find";
//     var cards = [];

// card schema
//	private String type;
//	private String description;
//	private Waypoint location;
//	private Date datetime;
//	private Long routeId;
//	private byte[] img;

    var chunk = 2;
    var start = Math.floor(Math.random()*(data.length-chunk));
    var distance = 0;
    for (var i=1; i<data.length; i++){
        var w1 = data[i-1];
        var w2 = data[i];
        distance+=calcDistance(w1.latitude,w1.longitude, w2.latitude,w2.longitude, "K");
    }

    var card = {};
	card.description = Math.floor(distance) + "K today";
	card.type = "summary";
	card.location = { 
	    latitude : data[0].latitude,
	    longitude : data[0].longitude
	}
	card.datetime = ""; 
	card.routeId = "";
	card.img = "";
    card.supertype = "place";
    callback(card);
    

    var subset = data.slice(start,start+chunk);
    var types = ["park","pool","library","food"];
    var descriptions = [];

    subset.forEach(function(waypoint){

         types.forEach(function(interest){
             console.log(interest);
            $http({
                method:'GET',
                url: esriRoot,
                cache: true,
                params: {
                    text: interest,
                    location: waypoint.longitude+","+waypoint.latitude,
                    distance: 10,
                    f: 'pjson'
                }
            }).success(function(result){
               result.locations.forEach(function(location){
		          var card = {};
                  if (descriptions.indexOf(location.name) === -1){
                      descriptions.push(location.name);
		              card.description = location.name;
		              card.type = interest;
		              card.location = { 
		                  latitude : location.feature.geometry.x,
		                  longitude : location.feature.geometry.y
		              }
		              card.datetime = ""; 
		              card.routeId = "";
		              card.img = "";
                      card.supertype = "place";
		              
                      callback(card);
                  }
               });

            }).error(function(error){
                console.log(error);
            });
         });

         var jobtypes = ["software"];
         jobtypes.forEach(function(job){
            $http({
                method:'GET',
                url: "https://jobs.github.com/positions.json",
                cache: true,
                params: {
                    lat: waypoint.latitude,
                    long: waypoint.longitude,
                }
            }).success(function(result){
               result.forEach(function(location){
		          var card = {};
                  if (descriptions.indexOf(location.name) === -1){
                      descriptions.push(location.name);
		              card.description = location.name;
		              card.type = jobtype;
		              card.location = { 
		                  latitude : waypoint.latitude,
		                  longitude : waypoint.longitude
		              }
		              card.datetime = ""; 
		              card.routeId = "";
		              card.img = "";
                      card.supertype = "job";
		              
                      callback(card);
                  }
               });

            }).error(function(error){
                console.log(error);
            });
         });
         
     });



     // var wayPoints = {
     //     routePoints:data,
     //     firstName:'woot',
     //     lastName:'woot'
     // };
     //$http({
     //    method: 'POST',
     //    url: 'http://localhost:8080/routes',
     //    data: wayPoints
     //},function(results){
     //    console.log(results);
     //},function(error){
     //    console.log(error);
     //});
    };

angular.module('starter.controller', [])

.controller('DeckCtrl', function($scope, $http) {

    var dates = ["May 25th","May 26th","May 27th","May 28th","May 29th","May 30th","May 31st"];
    $scope.days = [
        'history-05-25-2014.json','history-05-26-2014.json',
        'history-05-27-2014.json','history-05-28-2014.json',
        'history-05-29-2014.json', 'history-05-30-2014.json',
        'history-05-31-2014.json'
    ];

    $scope.cards = {};
    $scope.savedCards = [];

    //Current day key
    $scope.currentDay = $scope.days[0];
    $scope.humanReadableDay = dates[0];
    $scope.currentIndex = 0;

    // When a user swipes the deck
    $scope.slideHasChanged = function(index){
        $scope.loaded = false;
        $scope.currentIndex = index;

        $scope.humanReadableDay = dates[index];
        $scope.currentDay = $scope.days[index];

        // Load the trip data for the day
        if (!$scope.cards[$scope.currentDay]){
            $scope.cards[$scope.currentDay] = [];
            $.getJSON('data/'+$scope.currentDay,function(data){
                var onCards = function(card){
                    $scope.cards[$scope.currentDay].push(card);
                    if ($scope.cards[$scope.currentDay].length>1){
                        $scope.loaded = true;
                    }
                };
                processWayPoints($http, data, onCards);
            });
        }
    };

    // Default to first day in our list
    $scope.slideHasChanged(0);
    $scope.getIcon = function(activity){
        //var types = ["park","pool","library","food"];
        var images = {"park":'img/park.jpeg',
                      "pool":'img/swimming.jpeg',
                      "library":'img/library.png',
                      "summary":'img/walk.png',
                      "food":'img/food.png'}
        return images[activity]
    };


    $scope.saveForLater = function(card){
        var index=$scope.cards[$scope.currentDay].indexOf(card);
        var removed = $scope.cards[$scope.currentDay].splice(index, 1);
        $scope.savedCards.push(card);
    }

});

